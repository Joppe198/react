import './App.css'
import {HashRouter, Routes, Route } from 'react-router-dom'

import Valikko from './Pages/Valikko/Valikko'
import Etusivu from './Pages/Etusivu/Etusivu'
import Tietoa from './Pages/Tietoa/Tietoa'
import Harrastukset from './Pages/Harrastukset/Harrastukset'
import Koulutus from './Pages/Koulutus/Koulutus'
import Osaaminen from './Pages/Osaaminen/Osaaminen'
import ProjektiEsitys from './Pages/Projektit/Esitys'
import SaaEsitys from './Pages/Saa/Saasovellus'
import PaivanSaa from './Pages/Saa/PaivanSaa'
import SeitsemanPaivanSaa from './Pages/Saa/7Paivansaa'
import Tyot from './Pages/Tyot/Tyot'


function App() {
  return (
    <div className="app">
      <HashRouter>
        <Valikko />

        <Routes>
          <Route path="/" element={<Etusivu />}></Route>
          <Route path="/tietoa" element={<Tietoa />}></Route>
          <Route path="/harrastukset" element={<Harrastukset />}></Route>
          <Route path="/koulutus" element={<Koulutus />}></Route>
          <Route path="/osaaminen" element={<Osaaminen />}></Route>
          <Route path="/projektiesitys" element={<ProjektiEsitys />}></Route>
          <Route path="/saaesitys" element={<SaaEsitys />}></Route>
          <Route path="/paivansaa" element={<PaivanSaa />}></Route>
          <Route path="/seitsemanpaivansaa" element={<SeitsemanPaivanSaa />}></Route>
          <Route path="/tyot" element={<Tyot />}></Route>
        </Routes>
      </HashRouter>
    </div>
  );
}

export default App;
