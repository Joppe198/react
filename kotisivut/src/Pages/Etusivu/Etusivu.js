import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import './Etusivu.css'

const Home = () => {
    return (
      <Container>
        <Row>
          <Col sm={12}>
            <h1 className='Otsikko'>Etusivu</h1>
          </Col>
        </Row>
        <Row>
          <Col sm={12}>
            <p className='Otsikko'>Sivut olen kehittänyt React ja React-Bootstrap hyväksi käyttäen.</p>
          </Col>
        </Row>
      </Container>
    );
  }
  
  export default Home;