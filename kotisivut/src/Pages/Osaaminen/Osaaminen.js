import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ProgressBar from 'react-bootstrap/ProgressBar';
import ListGroup from 'react-bootstrap/ListGroup';
import Dropdown from 'react-bootstrap/Dropdown';

import './Osaaminen.css'

const Osaaminen = () => {
    return (
      <Container>
        <Row>
          <Col>
            <h1 className='Otsikko'>Osaamiseni</h1>
          </Col>
        </Row>
        <Row>
          <Col>
            <p className='Otsikko'>Seuraavissa alasveto valikoista löytyy saamani osaaminen eri ICT-alalta.</p>
            <p className='Otsikko'>Alla löytyy listaus millä tavalla Kerron oman osaamiseni tason.</p>
          </Col>
        </Row>
        <Row>
          <Col sm={{span: 4, offset: 4}}>
            <ListGroup>
              <ListGroup.Item>Pro<ProgressBar striped variant='danger' now={100} /></ListGroup.Item>
              <ListGroup.Item>Hyvä<ProgressBar striped variant='warning' now={75} /></ListGroup.Item>
              <ListGroup.Item>Kohtalainen<ProgressBar striped variant='success' now={50} /></ListGroup.Item>
              <ListGroup.Item>Alkeet<ProgressBar striped variant='info' now={25} /></ListGroup.Item>
            </ListGroup>
          </Col>
        </Row>
        <Row>
          <Col>
            <Dropdown className="d-inline mx-">
              <Dropdown.Toggle id="dropdown-autoclose-true">Käyttöjärjestelmä</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>Windows 11 <ProgressBar striped variant='warning' now={75} /></Dropdown.Item>
                <Dropdown.Item>Linux <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="d-inline mx-">
              <Dropdown.Toggle id="dropdown-autoclose-true">Mobiiliohjelmointi</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>Android <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Flutter <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
                <Dropdown.Item>Ionic <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="d-inline mx-">
              <Dropdown.Toggle id="dropdown-autoclose-true">Ohjelmointi kilet</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>C <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
                <Dropdown.Item>C++ <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
                <Dropdown.Item>C# <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Dart <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
                <Dropdown.Item>Java <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
                <Dropdown.Item>JSON <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Kotlin <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="d-inline mx-">
              <Dropdown.Toggle id="dropdown-autoclose-true">Ohjelmointi alustat</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>Android Studio <ProgressBar striped variant='warning' now={75} /></Dropdown.Item>
                <Dropdown.Item>Microsoft Visual Studio <ProgressBar striped variant='warning' now={75} /></Dropdown.Item>
                <Dropdown.Item>Microsoft Visual Studio Code <ProgressBar striped variant='warning' now={75} /></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="d-inline mx-">
              <Dropdown.Toggle id="dropdown-autoclose-true">Pelimoottorit</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>Construct 3 <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Unity <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Unreal Engine <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="d-inline mx-">
              <Dropdown.Toggle id="dropdown-autoclose-true">Piirto, Mallinus ja Valokuva Kehitys</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>Aseprite <ProgressBar striped variant='warning' now={75} /></Dropdown.Item>
                <Dropdown.Item>Aurora HDR <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
                <Dropdown.Item>Blender <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Corel Paint Shop Pro 2020 <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Inventor <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
                <Dropdown.Item>Luminar <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Photolemur <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="d-inline mx-">
              <Dropdown.Toggle id="dropdown-autoclose-true">Pilvipalvelut</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>Amatzon Web Service (AWS) <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
                <Dropdown.Item>Google Cloude Platform (GCP) <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="d-inline mx-">
              <Dropdown.Toggle id="dropdown-autoclose-true">Sovelluskehitys</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>Ansible <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
                <Dropdown.Item>Docker <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Docker Compose <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Git <ProgressBar striped variant='warning' now={75} /></Dropdown.Item>
                <Dropdown.Item>Gitlab <ProgressBar striped variant='warning' now={75} /></Dropdown.Item>
                <Dropdown.Item>Postman <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
                <Dropdown.Item>Robot Framework <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
                <Dropdown.Item>Vagrant <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="d-inline mx-">
              <Dropdown.Toggle id="dropdown-autoclose-true">Tietoliikenne Protokollat ja laitteet (Cisco)</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>ASA <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>BGP <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>DMZ <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>EIGRP <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Kytkin <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>MPLS <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>OSPF <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Palomuuri <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Reititin <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>VLAN <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>VPN <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="d-inline mx-">
              <Dropdown.Toggle id="dropdown-autoclose-true">Tietokannat</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>Mongo <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>MySQL <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>PostgreSQL <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="d-inline mx-">
              <Dropdown.Toggle id="dropdown-autoclose-true">Virtuaali todellisuus</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>Augment Reality (AR) <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Oculus Rift <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Virtual Reality (VR) <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Vuforial <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="d-inline mx-">
              <Dropdown.Toggle id="dropdown-autoclose-true">Web-Ohjelmointi</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>Angular <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
                <Dropdown.Item>Bootstrap <ProgressBar striped variant='warning' now={75} /></Dropdown.Item>
                <Dropdown.Item>CSS <ProgressBar striped variant='warning' now={75} /></Dropdown.Item>
                <Dropdown.Item>HTML <ProgressBar striped variant='warning' now={75} /></Dropdown.Item>
                <Dropdown.Item>JavaScript <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Node.js <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>NPM (Packet manager for the Node JavaScript platform) <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>React.js <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
                <Dropdown.Item>React-Bootstrap <ProgressBar striped variant='info' now={25} /></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="d-inline mx-">
              <Dropdown.Toggle id="dropdown-autoclose-true">Äänisuunittelu</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>Audacity <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Rytmik Studio <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="d-inline mx-">
              <Dropdown.Toggle id="dropdown-autoclose-true">Kartta Pohjat</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>Google Maps <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
                <Dropdown.Item>Leaflet.js <ProgressBar striped variant='success' now={50} /></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Col>
        </Row>
      </Container>
    );
  }
  
  export default Osaaminen;