import axios from 'axios';
import React, { useState, useEffect } from 'react'
import './App.css';

function Employee(props) {

  return (
    <div className="Employee">
      <img src={props.employee.image} alt=""></img>
      <h2>{props.employee.firstName} {props.employee.lastName}</h2>
      <p>Emails: {props.employee.email}</p>
      <p>Phone: {props.employee.phone}</p>
      <p>Title: {props.employee.title}</p>
      <p>Department: {props.employee.department}</p>
    </div>
  )
}
function App() {
  const [employees, setEmployees] = useState([])

  useEffect(()=> {
    axios
      .get('http://localhost:3001/employees')
      .then(response => {
        setEmployees(response.data)
      })
  }, [])

  const employeeItems = employees.map((employee,index) =>
  <Employee key={index} employee={employee}/>
);

  return (
    <div className="App">
      <div>
        {employeeItems}
      </div>
    </div>
  );
};





export default App;
