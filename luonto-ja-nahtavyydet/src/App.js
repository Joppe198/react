import React, { useEffect } from 'react';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import './App.css';
import axios from 'axios';

function App() {
  useEffect(() => {
    const map = L.map('map').setView([62.244, 25.755], 6);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© OpenStreetMap'
    }).addTo(map);

    const baseMaps = {
      'Open StreetMap': L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '© OpenStreetMap'
      })
    };

    const url1 = 'https://jonipeltomaa.gitlab.io/kotisivut/nahtavyydet.json';
    const url2 = 'https://jonipeltomaa.gitlab.io/kotisivut/nuotiopaikka.json';
    const url3 = 'https://jonipeltomaa.gitlab.io/kotisivut/museo.json';
    const url4 = 'https://jonipeltomaa.gitlab.io/kotisivut/torni.json';
    const url5 = 'https://jonipeltomaa.gitlab.io/kotisivut/luontopolku.json';
    const url6 = 'https://jonipeltomaa.gitlab.io/kotisivut/parkkipaikka.json';

    const markerIcons = {
      violet: L.icon({
        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-violet.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
      }),
      red: L.icon({
        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
      }),
      blue: L.icon({
        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
      }),
      green: L.icon({
        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
      }),
      orange: L.icon({
        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-orange.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
      }),
      grey: L.icon({
        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-grey.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
      })
    };

    const getColor = (id) => {
      switch (id) {
        case '1':
          return 'green';
        case '2':
          return 'blue';
        case '3':
          return 'red';
        case '4':
          return 'grey';
        case '5':
          return 'orange';
        case '6':
          return 'violet';
        default:
          return 'white';
      }
    };

    

    const nahtavyydet = L.geoJSON(null, {
      pointToLayer: function (feature, latlng) {
        return L.marker(latlng, {
          radius: 6,
          opacity: 1,
          color: getColor(feature.properties.id),
          fillColor: getColor(feature.properties.id),
          fillOpacity: 0.8,
          icon: markerIcons[getColor(feature.properties.id)]
        });
      },
      onEachFeature: function (feature, layer) {
        layer._leaflet_id = feature.properties.INFO;
        var popupContent = feature.properties.INFO;
        if (feature.properties && feature.properties.popupContent) {
          popupContent += feature.properties.popupContent;
        }
        layer.bindPopup(popupContent);
      }
    });

    axios
      .get(url1)
      .then(response => {
        const data = response.data;
        nahtavyydet.addData(data).addTo(map);
      })
      .catch(error => {
        console.log('Virhe ladattaessa JSON-tiedostoa:', error);
      });

      const nuotiopaikka = L.geoJson(null, {
	
        pointToLayer: function(feature, latlng) {
          return L.marker(latlng, {
          radius:6,
          opacity: 1,
          color:getColor(feature.properties.id),
          fillColor:  getColor(feature.properties.id),
          fillOpacity: 0.8,
          icon: markerIcons[getColor(feature.properties.id)]
          });  
        },
        
        onEachFeature: function (feature, layer) {
          layer._leaflet_id = feature.properties.INFO;
          var popupContent = feature.properties.INFO;
          if (feature.properties && feature.properties.popupContent) {
            popupContent += feature.properties.popupContent;
          }
            layer.bindPopup(popupContent);
        }
      });

      axios
      .get(url2)
      .then(response => {
        const data = response.data;
        nuotiopaikka.addData(data).addTo(map);
      })
      .catch(error => {
        console.log('Virhe ladattaessa JSON-tiedostoa:', error);
      });

      var museo = L.geoJson(null, {
	
        pointToLayer: function(feature, latlng) {
          return L.marker(latlng, {
            radius:6,
            opacity: 1,
            color:getColor(feature.properties.id),
            fillColor:  getColor(feature.properties.id),
            fillOpacity: 0.8,
            icon: markerIcons[getColor(feature.properties.id)]
          });  
        },
        
        onEachFeature: function (feature, layer) {
          layer._leaflet_id = feature.properties.INFO;
          var popupContent = feature.properties.INFO;	
          if (feature.properties && feature.properties.popupContent) {
            popupContent += feature.properties.popupContent;
          }
            layer.bindPopup(popupContent);
        }
      });

      axios
      .get(url3)
      .then(response => {
        const data = response.data;
        museo.addData(data).addTo(map);
      })
      .catch(error => {
        console.log('Virhe ladattaessa JSON-tiedostoa:', error);
      });

      const torni = L.geoJson(null, {
	
        pointToLayer: function(feature, latlng) {
          return L.marker(latlng, {
            radius:6,
            opacity: 1,
            color:getColor(feature.properties.id),
            fillColor:  getColor(feature.properties.id),
            fillOpacity: 0.8,
            icon: markerIcons[getColor(feature.properties.id)]
          });
        },
        
        onEachFeature: function (feature, layer) {
          layer._leaflet_id = feature.properties.INFO;
          var popupContent = feature.properties.INFO;
          if (feature.properties && feature.properties.popupContent) {
            popupContent += feature.properties.popupContent;
          }
            layer.bindPopup(popupContent);
        }
      });
      
      axios
      .get(url4)
      .then(response => {
        const data = response.data;
        torni.addData(data).addTo(map);
      })
      .catch(error => {
        console.log('Virhe ladattaessa JSON-tiedostoa:', error);
      });

      const luontopolku = L.geoJson(null, {
        pointToLayer: function(feature, latlng) {
          return L.marker(latlng, {
            radius:6,
            opacity: 1,
            color:getColor(feature.properties.id),
            fillColor:  getColor(feature.properties.id),
            fillOpacity: 0.8,
            icon: markerIcons[getColor(feature.properties.id)]
          });
        },
      
        onEachFeature: function (feature, layer) {
          layer._leaflet_id = feature.properties.INFO;
          var popupContent = feature.properties.INFO;
          if (feature.properties && feature.properties.popupContent) {
            popupContent += feature.properties.popupContent;
          }
            layer.bindPopup(popupContent);
        }
      });

      axios
      .get(url5)
      .then(response => {
        const data = response.data;
        luontopolku.addData(data).addTo(map);
      })
      .catch(error => {
        console.log('Virhe ladattaessa JSON-tiedostoa:', error);
      });

      const parkkipaikka = L.geoJson(null, {
	
        pointToLayer: function(feature, latlng) {
          return L.marker(latlng, {
            radius:6,
            opacity: 1,
            color:getColor(feature.properties.id),
            fillColor:  getColor(feature.properties.id),
            fillOpacity: 0.8,
            icon: markerIcons[getColor(feature.properties.id)]
          });
        },
        
        onEachFeature: function (feature, layer) {
          layer._leaflet_id = feature.properties.INFO;
          var popupContent = feature.properties.INFO;
          if (feature.properties && feature.properties.popupContent) {
            popupContent += feature.properties.popupContent;
          }
            layer.bindPopup(popupContent);
        }
      });

      axios
      .get(url6)
      .then(response => {
        const data = response.data;
        parkkipaikka.addData(data).addTo(map);
      })
      .catch(error => {
        console.log('Virhe ladattaessa JSON-tiedostoa:', error);
      });


    const overlayMaps = {
      '<i class="fa-solid fa-person-hiking"></i> Luontopolkku': luontopolku,
      '<i class="fa-solid fa-square-parking"></i> Parkkipaikka': parkkipaikka,
      '<i class="fa-solid fa-fire"></i> Nuotiopaikka': nuotiopaikka,
      '<i class="fa-solid fa-landmark"></i> Museo': museo,
      '<i class="fa-solid fa-tower-observation"></i> Näkötorni': torni,
      '<i class="fa-solid fa-camera"></i> Nahtavyydet': nahtavyydet
    };

    L.control.layers(baseMaps, overlayMaps).addTo(map);

    return () => {
      map.remove();
    };
  }, []);

  return <div id="map"></div>;
}

export default App;