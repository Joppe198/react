import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

function Valikko() {
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="#/">Etusivu</Nav.Link>
            <Nav.Link href="#/tietoa">Tietoa Minusta</Nav.Link>
            <Nav.Link href="#/harrastukset">Harrastukset</Nav.Link>
            <Nav.Link href="#/koulutus">Koulutus</Nav.Link>
            <Nav.Link href="#/osaaminen">Osaaminen</Nav.Link>
            <Nav.Link href="#/tyot">Työt</Nav.Link>
            <NavDropdown title="Projektit" id="collasible-nav-dropdown">
              <NavDropdown.Item href="#/projektiesitys">Tietoa Sovelluksistani</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="Sää" id="collasible-nav-dropdown">
              <NavDropdown.Item href="#/saaesitys">Tietoa Sää Sovelluksesta</NavDropdown.Item>
              <NavDropdown.Item href="#/paivansaa">Päivän Sää</NavDropdown.Item>
              <NavDropdown.Item href="#/seitsemanpaivansaa">7 Päivän Sää</NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Valikko;